$(document).ready(function () {
  var openMenu = $('.open-menu'),
    closeMenu = $('.close-menu'),
    sideBtn = $('.side-btn'),
    overlay = $('.overlay'),
    isClosed = false;

  openMenu.click(function () {
    hamburger();
  });

  closeMenu.click(function () {
    hamburger();
  });

  function hamburger() {
    if (isClosed == true) {
      overlay.hide();
      openMenu.removeClass('is-open');
      openMenu.addClass('is-closed');
      sideBtn.toggleClass('d-none');
      isClosed = false;
    } else {
      overlay.show();
      openMenu.removeClass('is-closed');
      openMenu.addClass('is-open');
      sideBtn.toggleClass('d-none');
      isClosed = true;
    }
  }

  $('.open-menu').click(function () {
    $('#wrapper').toggleClass('toggled');
  });
  $('.close-menu').click(function () {
    $('#wrapper').toggleClass('toggled');
  });
});

// ****************FOR TESTING ONLY *********************//
let loginBtn = document.querySelector('.btn-login');
if (loginBtn) {
  loginBtn.addEventListener('click', (e) => {
    e.preventDefault();
    window.location = '../views/welcome.html';
  });
}
// *********************END *********************//

// custome select
let x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName('custom-select');

l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName('select')[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement('DIV');
  a.setAttribute('class', 'select-selected');
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement('DIV');
  b.setAttribute('class', 'select-items select-hide');

  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement('DIV');
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener('click', function (e) {
      /*when an item is clicked, update the original select box,
        and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName('select')[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName('same-as-selected');
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute('class');
          }
          this.setAttribute('class', 'same-as-selected');
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener('click', function (e) {
    /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle('select-hide');
    this.classList.toggle('select-arrow-active');
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x,
    y,
    i,
    xl,
    yl,
    arrNo = [];
  x = document.getElementsByClassName('select-items');
  y = document.getElementsByClassName('select-selected');
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i);
    } else {
      y[i].classList.remove('select-arrow-active');
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add('select-hide');
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener('click', closeAllSelect);
// end of custome select

// change input border color on change
function changeBorder(e) {
  e = e || window.event;
  let target = e.target || e.srcElement;
  let changedInput = target.parentElement.previousSibling;
  changedInput.style.border = '1px solid #bb8a30';
}

function showDetails(e) {
  let bankDetails = document.querySelectorAll('.tujuan');
  let selectedOption = document.querySelector('#tujuan .select-selected');
  selectedOption.setAttribute('onclick', 'showDetails()');
  let bankName = selectedOption.innerHTML;
  if (bankName !== 'PILIH BANK') {
    bankDetails.forEach((details) => {
      let id = details.dataset.id;
      if (id === bankName.toLowerCase()) {
        details.classList.toggle('d-none');
      }
    });
  }
}
if ($('#tujuan').length > 0) {
  showDetails();
}
// end
let inputs = document.querySelectorAll('input.form-control');
inputs.forEach((input) => {
  input.addEventListener('change', function () {
    this.style.borderColor = 'rgb(187, 138, 48)';
  });
});

function showInputFields() {
  let bankBtn = document.querySelector('#bank-btn');
  let socialBtn = document.querySelector('#social-btn');
  let bankInput = document.querySelector('#bank-input');
  let socialInput = document.querySelector('#social-input');

  bankBtn.addEventListener('click', function () {
    bankInput.classList.remove('d-none');
    bankInput.classList.add('d-flex');
  });
  socialBtn.addEventListener('click', function () {
    socialInput.classList.remove('d-none');
    socialInput.classList.add('d-flex');
  });
}
if ($('#bank-input').length > 0) {
  showInputFields();
}

// tab congtrol using radio buttons
$(document).ready(function () {
  $('input[name="metodePenyetoran"]').click(function () {
    $(this).tab('show');
    $(this).removeClass('active');
  });
});
// end oftab congtrol using radio buttons

// activate change user ALIAS
function editAlias() {
  $('#alias').prop('disabled', false);
}
// end of activate change user ALIAS

// edit social details
function editSocial(btn) {
  btn.parentElement.previousElementSibling.disabled = false;
}
// end of edit social details
